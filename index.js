class Tab {
  constructor(listOfOptions) {
    this.listOfOptions = listOfOptions;
  }

  getTabHeader() {
    let tabHeader = document.createElement("div");
    tabHeader.className = "tab__items";

    this.listOfOptions.forEach((option, index) => {
      let tabItemElem = document.createElement("div");
      tabItemElem.className = "tab__item";

      tabItemElem.textContent = option.title;
      tabItemElem.tabIndex = index + 1;
      tabItemElem.dataset.tabIndex = index + 1;

      if (index === 0) {
        tabItemElem.classList.add("_current");
        this.startContent = option.data;
        tabItemElem.tabIndex = -1;
      }

      tabHeader.appendChild(tabItemElem);
    });

    return tabHeader;
  }

  getTabContentElem() {
    let tabContentElem = document.createElement("div");
    tabContentElem.className = "tab__content";
    tabContentElem.textContent = this.startContent;

    return tabContentElem;
  }

  getOptionData(optionTitle) {
    let data;

    this.listOfOptions.forEach((option) => {
      if (option.title === optionTitle) {
        data = option.data;
      }
    });

    return data;
  }

  render() {
    let tabElem = document.createElement("div");

    tabElem.appendChild(this.getTabHeader());
    tabElem.appendChild(this.getTabContentElem());

    return tabElem;
  }
}

// Входные данные
const tab = new Tab([
  {
    title: "Общая информация",
    data: `Какая-то личная информация о пользователе`,
  },
  {
    title: "История",
    data: `Какая-то история действий пользователя`,
  },
  {
    title: "Друзья",
    data: `Какие-то люди`,
  },
]);

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById("tab").appendChild(tab.render());

document.addEventListener("click", (event) => {
  const { target } = event;

  if (isTabItem(target)) {
    setCurrentOption(target);
  }
});

function setCurrentOption(target) {
  const currentOption = getCurrentOption();

  if (target !== currentOption) {
    target.classList.add("_current");
    target.tabIndex = -1;
    setContent(target.textContent);
    currentOption.classList.remove("_current");
    currentOption.tabIndex = currentOption.dataset.tabIndex;
  }
}

function isTabItem(target) {
  return target.classList.contains("tab__item");
}

function getCurrentOption() {
  let tabOptions = document.querySelectorAll(".tab__item");
  let currentOption;

  tabOptions.forEach((option) => {
    if (option.classList.contains("_current")) {
      currentOption = option;
    }
  });

  return currentOption;
}

function setContent(optionTitle) {
  const contentElem = document.querySelector(".tab__content");

  const data = tab.getOptionData(optionTitle);

  contentElem.textContent = data;
}

document.activeElement.addEventListener("keyup", (event) => {
  if (event.keyCode === 13) {
    setCurrentOption(event.target);
  }
});
